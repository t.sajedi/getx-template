import 'dart:developer';

import 'package:get/get.dart';
import 'package:test/app/controller/home_controller.dart';
import 'package:test/app/routes/app_pages.dart';

class LandingController extends GetxController {
  final _tabIndex = 0.obs;
  set setIndex(value) => this._tabIndex.value = value;
  get tabIndex => this._tabIndex.value;

  onTabTapped(int index) {
    log(index.toString());
    if (index == 2) {
      Get.toNamed(Routes.MOZ)!.then((value) {
        // final HomeController ctrl = Get.find();
        // ctrl.onReady();
      });
    } else {
      setIndex = index;
    }

    // update();
  }
}
