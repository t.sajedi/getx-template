import 'dart:async';
import 'dart:developer';

import 'package:get/get.dart';
import '/app/middleware/authentication_middleware.dart';

class HomeController extends GetxController {
  final now = DateTime.now().obs;

  @override
  void onInit() {
    super.onInit();
    log('homeonInit');
    AuthenticationMiddleware.loginRequired();
  }

  @override
  void onReady() {
    super.onReady();
    printInfo(info: 'onReady');
    log("home on reaaaaaaaaaaaaaaaady");
    Timer.periodic(
      Duration(seconds: 1),
      (timer) {
        now.value = DateTime.now();
        update();
      },
    );
  }

  @override
  void onClose() {
    super.onClose();
  }
}
