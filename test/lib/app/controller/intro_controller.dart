import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../routes/app_pages.dart';

class IntroController extends GetxController {
  final currentPage = 0.obs;
  final pageController = PageController(initialPage: 0).obs;
  int totalPage = 3;

  final _duration = Duration(milliseconds: 500);
  final _curve = Curves.linearToEaseOut;

  @override
  void onReady() {
    log("intro page onReaaaaaaaaaaaaaaady");
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  nextPage() {
    if (pageController.value.page == totalPage - 1)
      _redirect();
    else {
      pageController.value.nextPage(
        duration: _duration,
        curve: _curve,
      );

      currentPage.value++;
      update();
    }
  }

  previousPage() {
    if (pageController.value.page != 0) {
      pageController.value.previousPage(
        duration: _duration,
        curve: _curve,
      );
      currentPage.value--;
      update();
    }
  }

  _redirect() {
    Get.offAndToNamed(Routes.Landing);
  }
}
