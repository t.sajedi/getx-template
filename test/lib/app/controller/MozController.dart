import 'dart:developer';

import 'package:get/get.dart';

class Mozcontroller extends GetxController {
  RxInt index = 0.obs;

  getIndex() => index.value;

  increament(int newindex) {
    index.value = newindex;
    update();
  }

  @override
  void onInit() {
    printInfo(info: "moz controller on iniiiiiiiiiiiiiiiiiiit");
    super.onInit();
  }

  @override
  void onReady() {
    print("moz controller reaaaaaaaaaaaaaaaaaaaaaaaaaaaaaady");
    super.onReady();
  }
}
