import 'package:get/get.dart';

import '../helpers/log_helper.dart';

class LifeCycleController extends SuperController {
  @override
  void onDetached() {
    LogHelper.log('onDetached');
  }

  @override
  void onInactive() {
    LogHelper.log('onInactive');
  }

  @override
  void onPaused() {
    LogHelper.log('onPaused');
  }

  @override
  void onResumed() {
    LogHelper.log('onResumed');
  }
}
