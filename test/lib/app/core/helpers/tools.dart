import 'dart:io';

extension AssetsConverter on String {
  get asset {
    return Platform.isAndroid || Platform.isIOS ? 'assets/$this' : this;
  }
}
