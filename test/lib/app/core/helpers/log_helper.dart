import 'package:get/get.dart';
import 'package:intl/intl.dart' as intl;

class LogHelper extends GetXState {
  String get _time =>
      intl.DateFormat.yMEd('fa_FA').add_jms().format(DateTime.now());
  // DateTime.now().toString();

  LogHelper.log(dynamic message) {
    Get.log('Logger $_time : ${message.toString()}');
  }

  LogHelper.initial() {
    Get.log(
        'Running APP version 1.0.0+1 on Xioami Poco X3\nInitial Locale ${Get.fallbackLocale}\n$_time');
  }
}
