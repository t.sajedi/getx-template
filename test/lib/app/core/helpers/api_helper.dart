import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:get/get_connect/connect.dart';

import 'log_helper.dart';

class APIHelper extends GetConnect {
  static const String protocol = 'https://';
  static const String domain = '127.0.0.1';
  static const String port = ':8080';

  Future<dynamic> callAPI(
    String url, {
    required Method method,
    Map<String, String>? headers,
    Map<String, dynamic>? query,
    Map? body,
    FormData? form,
  }) async {
    LogHelper.log('Calling ** Post ** API: $url');

    // open response object
    Response? response;

    // create URI
    String uri = _baseUrl + url;

    // set default headers
    headers!.addAll({
      HttpHeaders.contentTypeHeader: 'application/json',
    });

    // send request and check timeout & no internet connection
    try {
      switch (method) {
        case Method.POST:
          {
            response = await post(
              uri,
              form ?? body,
              headers: headers,
              query: query,
            );
            break;
          }

        case Method.PUT:
          {
            response = await put(
              uri,
              form ?? body,
              headers: headers,
              query: query,
            );
            break;
          }

        case Method.GET:
          {
            response = await get(
              uri,
              headers: headers,
              query: query,
            );
            break;
          }

        case Method.DELETE:
          {
            response = await delete(
              uri,
              headers: headers,
              query: query,
            );
            break;
          }
        default:
          {
            LogHelper.log('Undefined Method');
            return;
          }
      }
    } on SocketException {
      response = Response(
        body: jsonEncode({'msg': 'No Internet Connection'}),
        statusCode: HttpStatus.clientClosedRequest,
      );
    } on TimeoutException {
      response = Response(
        body: jsonEncode({'msg': 'Request Timeout'}),
        statusCode: HttpStatus.requestTimeout,
      );
      LogHelper.log('${response.statusCode} : ${response.body}');
    }

    return response;
  }

  GetSocket callSocket(String url) {
    return socket(
      _baseUrl + url,
    );
  }

  String get _baseUrl => protocol + domain + port;
}

enum Method {
  POST,
  PUT,
  GET,
  DELETE,
}
