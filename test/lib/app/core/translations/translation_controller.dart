import 'package:flutter/material.dart';
// import 'package:get/get.dart' as storage;
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'en_US/en_US.dart';
import 'fa_FA/fa_FA.dart';

class TranslationController extends GetxController {
  static TranslationController get to => Get.find();

  static const _storageKey = 'locale';

  static Map<String, Map<String, String>> translations = {
    'en_US': enUs,
    'fa_FA': faFA
  };

  static const Iterable<Locale> supportedLocales = [
    const Locale('en', 'US'),
    const Locale('fa', 'FA'),
  ];

  GetStorage? storage;
  Locale? _locale;
  Locale get locale => _locale!;

  @override
  void onInit() {
    storage = GetStorage();
    getLocaleStorage();

    super.onInit();
  }

  setLocaleStorage(Locale locale) async {
    Get.updateLocale(locale);

    storage!.write(_storageKey, locale.toString());
  }

  getLocaleStorage() async {
    Locale locale;
    String? localeText = storage!.read(_storageKey) ?? null;
    print('Load Locale $localeText');
    if (localeText != null) {
      var _splits = localeText.split('_');
      String _languageCode = _splits.elementAt(0);
      String _countryCode = _splits.elementAt(1);

      locale = Locale(_languageCode, _countryCode);
    } else
      locale = supportedLocales.elementAt(0);

    setLocaleStorage(locale);
  }

  Future<void> changeLocale({required Locale locale}) async {
    Get.updateLocale(locale);
    storage!.write(_storageKey, locale.toString());
  }
}
