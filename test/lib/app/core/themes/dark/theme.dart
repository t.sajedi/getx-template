import 'package:flutter/material.dart';

final ThemeData darkThemeData = ThemeData(
  primaryColor: Colors.yellow,
  accentColor: Colors.yellowAccent,
  splashColor: Colors.yellowAccent,
  highlightColor: Colors.yellow,
  scaffoldBackgroundColor: Color(0xff212121),
  bottomNavigationBarTheme: BottomNavigationBarThemeData(
    unselectedItemColor: Colors.yellow,
    selectedItemColor: Colors.black,
    backgroundColor: Colors.orange,
    type: BottomNavigationBarType.fixed
  ),
  fontFamily: 'Georgia',
  textTheme: TextTheme(
    headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
  ),
);
