import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'dark/theme.dart';
import 'light/theme.dart';

class ThemeController extends GetxController {
  static ThemeController get to => Get.find();

  static const _storageKey = 'theme';
  GetStorage? storage;
  ThemeMode? _themeMode;
  ThemeMode get themeMode => _themeMode!;

  ThemeData? _themeData;
  ThemeData get themeData => _themeData!;

  @override
  void onInit() {
    storage = GetStorage();
    getThemeModeStorage();

    super.onInit();
  }

  Future<void> changeTheme() async {
    if (_themeMode == ThemeMode.light) {
      Get.changeTheme(darkThemeData);
      _themeMode = ThemeMode.dark;
    } else {
      Get.changeTheme(lightThemeData);
      _themeMode = ThemeMode.light;
    }
    update();

    storage!.write(_storageKey, themeMode.toString().split('.')[1]);
  }

  setThemeModeStorage(ThemeMode themeMode) async {
    if (themeMode == ThemeMode.light) {
      Get.changeTheme(lightThemeData);
      _themeMode = ThemeMode.light;
      storage!.write(_storageKey, _themeMode.toString().split('.')[1]);
    } else {
      Get.changeTheme(darkThemeData);
      _themeMode = ThemeMode.dark;
      storage!.write(_storageKey, _themeMode.toString().split('.')[1]);
    }
  }

  getThemeModeStorage() async {
    ThemeMode themeMode;
    String? themeText = storage!.read(_storageKey) ?? null;
    print('Load Theme $themeText');
    try {
      themeMode =
          ThemeMode.values.firstWhere((e) => describeEnum(e) == themeText);
    } catch (e) {
      themeMode = ThemeMode.light;
    }

    setThemeModeStorage(themeMode);
  }
}
