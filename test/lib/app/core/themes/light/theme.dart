import 'package:flutter/material.dart';

final ThemeData lightThemeData = ThemeData(
  primaryColor: Colors.purple,
  accentColor: Colors.purpleAccent,
  splashColor: Colors.purpleAccent,
  highlightColor: Colors.purple,
  scaffoldBackgroundColor: Colors.white,
  bottomNavigationBarTheme: BottomNavigationBarThemeData(
    unselectedItemColor: Colors.purple,
    selectedItemColor: Colors.black,
    backgroundColor: Colors.yellow,
    type: BottomNavigationBarType.fixed,
  ),
  fontFamily: 'Georgia',
  textTheme: TextTheme(
    headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
  ),
);
