import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:test/app/controller/MozController.dart';

class MozView extends GetView<Mozcontroller> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Obx(() {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(controller.getIndex().toString()),
            ElevatedButton(
              child: Text("tap"),
              onPressed: () {
                controller.increament(controller.getIndex() + 1);
              },
            ),
          ],
        );
      }),
    );
  }
}
