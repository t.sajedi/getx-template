import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/app/views/home/home_view.dart';
import '/app/views/setting/setting_view.dart';
import '../../controller/landing_controller.dart';

import './widgets/bottom_navigation_bar.dart';

class LandingView extends GetView<LandingController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () => IndexedStack(
          index: controller.tabIndex,
          children: [
            HomeView(),
            SettingView(),
            // HomeView(),
            // HomeView(),
            // Scaffold(backgroundColor: Colors.orange),
            Scaffold(backgroundColor: Colors.pink),
            Scaffold(backgroundColor: Colors.lightGreen),
          ],
        ),
      ),
      bottomNavigationBar: NavigationBar(controller),
    );
  }
}
