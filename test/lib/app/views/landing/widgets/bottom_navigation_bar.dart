import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/app/controller/landing_controller.dart';

class NavigationBar extends GetWidget {
  final LandingController? controller;

  NavigationBar(this.controller);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<LandingController>(
      init: controller,
      builder: (s) => BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Inicio',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'Pesquisa',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Perfil',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Perfil',
          ),
        ],
        currentIndex: s.tabIndex,
        onTap: (index) => s.onTabTapped(index),
      ),
    );
  }
}
