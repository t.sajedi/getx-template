import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../controller/splash_controller.dart';

class SplashView extends GetView<SplashController> {
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
        body: Center(
          child: Text(
            'Splash Layout ${controller.time.toString()}',
            style: TextStyle(fontSize: 36.0),
          ),
        ),
      ),
    );
  }
}
