import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../views/first_intro_view.dart';
import '../views/second_intro_view.dart';
import '../views/third_intro_view.dart';

import '../../../controller/intro_controller.dart';

class IntroView extends GetView<IntroController> {
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
        body: Column(
          children: [
            Expanded(
              child: PageView.builder(
                physics: NeverScrollableScrollPhysics(),
                controller: controller.pageController.value,
                itemCount: 3,
                itemBuilder: (context, index) {
                  switch (index) {
                    case 0:
                      return FirstIntroView();
                    case 1:
                      return SecondIntroView();
                    case 2:
                      return ThirdIntroView();
                    default:
                      return Container();
                  }
                },
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton(
                  onPressed: controller.currentPage.value != 0
                      ? controller.previousPage
                      : null,
                  child: Icon(Icons.arrow_back_ios_new_rounded),
                ),
                Text(controller.currentPage.string),
                ElevatedButton(
                  onPressed: controller.nextPage,
                  child: Icon(Icons.arrow_forward_ios_rounded),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
