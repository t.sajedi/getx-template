import 'package:get/get.dart';

import '../controller/landing_controller.dart';
import 'home_binding.dart';
import 'setting_binding.dart';

class LandingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LandingController>(
      () => LandingController(),
    );

    HomeBinding().dependencies();
    SettingBinding().dependencies();
  }
}
