import 'package:get/get.dart';

import '../controller/setting_controller.dart';

class SettingBinding extends Bindings {
  @override
  void dependencies() {
    print("SettingBinding dependencies");
    Get.lazyPut<SettingController>(
      () => SettingController(),
    );
  }
}
