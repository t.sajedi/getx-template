import 'package:get/get.dart';

import '../controller/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    print("HomeBinding dependencies");
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
  }
}
