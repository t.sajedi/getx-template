import 'package:get/get.dart';
import 'package:test/app/controller/MozController.dart';

class MozBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<Mozcontroller>(() => Mozcontroller());
  }
}
